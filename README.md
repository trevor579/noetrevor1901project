## Vegan Easy

Vegan Easy is an iOS app that displays data about vegan and vegan friendly restaurants. Data from Google is used as well as data given by the community.

---

**What it is**

Vegan Easy is a community driven project. Users can see restaurant listings and menu items posted by members of the community.

User can:

- Add new restauant listing.
- Add a menu item with a description on how to make the item vegan (if applicable)
- Edit menu items or restaurant listings if found to be incomplete or inaccurate.
- Write reviews on restaurants.
- Edit or delete their own reviews.
