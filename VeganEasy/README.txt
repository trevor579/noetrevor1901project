{\rtf1\ansi\ansicpg1252\cocoartf1671\cocoasubrtf200
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural\partightenfactor0

\f0\fs24 \cf0 README\
\
IMPORTANT: OPEN \'93VeganEasy.xcworkspace\'94 NOT \'93VeganEasy.xcodeproj\'94\
 - Firebase core framework and database are only installed in the .xcworkspace file\
\
Installation Instructions:\
 	\
*Currently this is not on the App Store, so you must side load it if you were to install it to your device.\
 \
 - Open the file named VeganEasy.xcworkspace in Xcode\
 - Plug in your iOS device\
 - Select your device from the list next to the play and stop buttons at the top left of the screen\
 - Unlock your device and press the play button\
\
\
Hardware Requirements:\
\
*for Xcode file\
 - a Mac running macOS 10.13.6 or later\
 - an internet connection\
\
*for installation to iOS device\
 - an iOS device running iOS 12.1 or later\
\
\
Log in requirements:\
\
You can create an account by using your first and last name, your email, a password, and your location. \
\
\
Known Bugs:\
\
None at this time\
\
\
}