//
//  Restaurant.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/26/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import Foundation
import UIKit

class Restaurant {
    
    //Stored Properties
    var name: String
    var address: String
    var description: String
    var dietType: String
    var menu: [Item]
    var reviews: [Review]
    
    //return average review
    var aveReview: Int {
        get {
            var aveDouble: Double = 0
            var ave = 0
            for review in reviews {
                aveDouble += Double(review.rating)
            }
            
            //just return 0 if there are no ratings to avoid crash
            if reviews.count > 0 {
                aveDouble /= Double(reviews.count)
                //round using "schoolbook rounding" rather than Int default rounding
                ave = Int(aveDouble.rounded())
                return ave
            } else {
                return 0
            }
            
            
        }
    }
    
    func getReviewsByUser(userEmail: String) -> [Review] {
        var userReviews = [Review]()
        for review in reviews {
            if review.userEmail == userEmail {
                userReviews.append(review)
            }
        }
        return userReviews
    }
    
    //returns the highest price in the menu
    var highPrice: String {
        get {
            var thePrice: Double = menu[0].price
            
            for item in menu {
                if item.price > thePrice {
                    thePrice = item.price
                }
            }
            
            return "$\(thePrice)"
        }
    }
    
    //returns the lowest price in the menu
    var lowPrice: String {
        get {
            var thePrice: Double = menu[0].price
            
            for item in menu {
                if item.price < thePrice {
                    thePrice = item.price
                }
            }
            
            return "$\(thePrice)"
        }
    }
    
    //Initializers
    init(name: String, address: String, description: String, dietType: String, menu: [Item], reviews: [Review]) {
        self.name = name
        self.address = address
        self.description = description
        self.dietType = dietType
        self.menu = menu
        self.reviews = reviews
    }
    
}
