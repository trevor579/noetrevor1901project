//
//  Item.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/27/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import Foundation


class Item {
    var name: String
    var price: Double
    var description: String
    
    var priceString: String {
        get {
            return "$\(price.description)"
        }
    }
    
    init(name: String, price: Double, description: String) {
        self.name = name
        self.price = price
        self.description = description
    }
}
