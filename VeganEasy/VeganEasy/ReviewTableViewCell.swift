//
//  ReviewTableViewCell.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/31/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var reviewText: UITextView!
    @IBOutlet weak var starsImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
