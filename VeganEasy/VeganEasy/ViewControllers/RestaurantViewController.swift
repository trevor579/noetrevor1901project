//
//  RestaurantViewController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/29/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit
import FirebaseDatabase

class RestaurantViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var ref: DatabaseReference!
    var restaurant: Restaurant!
    var currUser: User!
    var userLocation: String!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dietTypeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var ratingImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = restaurant.name
        
        addressLabel.text = restaurant.address
        dietTypeLabel.text = restaurant.dietType
        descriptionLabel.text = restaurant.description
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userLocation = currUser.location.replacingOccurrences(of: " ", with: "")
        
        //make sure that all information is up to date
        restaurant.menu.removeAll()
        ref = Database.database().reference()
        ref.child("locations/\(userLocation!)/\(restaurant.name.replacingOccurrences(of: " ", with: ""))/menu").observeSingleEvent(of: .value) { (snapshot) in
            if let menuData = snapshot.value as? [String: Any] {
                for (_,data) in menuData {
                    let item = data as! [String: Any]
                    let name = item["name"] as! String
                    let price = item["price"] as! Double
                    let description = item["description"] as! String
                    
                    let newItem = Item(name: name, price: price, description: description)
                    self.restaurant.menu.append(newItem)
                }
            }
            self.tableview.reloadData()
            
            //set average rating image
            switch self.restaurant.aveReview {
            case 1:
                self.ratingImg.image = UIImage(named: "1starImage")
            case 2:
                self.ratingImg.image = UIImage(named: "2starImage")
            case 3:
                self.ratingImg.image = UIImage(named: "3starImage")
            case 4:
                self.ratingImg.image = UIImage(named: "4starImage")
            case 5:
                self.ratingImg.image = UIImage(named: "5starImage")
            default:
                print("Invalid rating")
            }
        }
    }

    // MARK: - Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurant.menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Reusable") as! MenuItemTableViewCell
        let currItem = restaurant.menu[indexPath.row]
        
        cell.itemName.text = currItem.name
        cell.itemPrice.text = currItem.priceString
        cell.itemDescription.text = currItem.description
        
        return cell
    }
    
    
    // MARK: - Navigation
    
    
    @IBAction func addItemButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "addOrEditMenuItem", sender: sender)
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        //check which view controller information is getting sent to
        if let destination = segue.destination as? MenuItemViewController{
            //send restaurant data/user location always
            destination.newRestaurant = restaurant
            destination.userLocation = userLocation
            
            //check if "Add" button pressed or if table view cell was pressed
            if let indexPath = tableview.indexPathForSelectedRow {
                destination.menuItem = restaurant.menu[indexPath.row]
                destination.isEditingHappening = true
            } else {
                //don't send a menu item
            }
        }
        else if let destination = segue.destination as? ReviewsTableViewController {
            destination.restaurant = restaurant
            destination.currUser = currUser
        }
        
        
        
    }
 

}
