//
//  ProfileViewController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/26/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    var currUser: User!

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.title = "Profile"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tabs = tabBarController as! BaseTabBarController
        
        currUser = tabs.currUser

        nameLabel.text = currUser.fullName
        locationLabel.text = currUser.location
        // Do any additional setup after loading the view.
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! NewRestaurantViewController
        destination.userLocation = currUser.location.replacingOccurrences(of: " ", with: "")
    }
 

}
