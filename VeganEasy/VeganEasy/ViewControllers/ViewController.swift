//
//  ViewController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/19/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var fNameField: UITextField!
    @IBOutlet weak var lNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var locationField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    var ref :DatabaseReference!
    
    var currUser: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.hideKeyboardWhenTappedAround()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag < 4 {
            let nextTextField = textField.superview?.viewWithTag(textField.tag + 1)
            nextTextField?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            signUpButton(signUpButton)
        }
        
        return true
    }
    
    //saves inputted data to database
    @IBAction func signUpButton(_ sender: UIButton) {
        ref = Database.database().reference()
        
        //makes sure that data is in the text fields
        if fNameField.text == "" || lNameField.text == "" || emailField.text == "" || passwordField.text == "" || locationField.text == "" {
            let alert = UIAlertController(title: "Incomplete", message: "Please fill out all text fields", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            ref.child("users").childByAutoId().setValue(["fname" : fNameField.text, "lname" : lNameField.text, "email" : emailField.text, "password" : passwordField.text, "location" : locationField.text])
            currUser = User(fname: fNameField.text!, lname: lNameField.text!, location: locationField.text!, email: emailField.text!)
            performSegue(withIdentifier: "segueToLogIn", sender: sender)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! LogInViewController
        destination.currUser = self.currUser
    }
    
}

