//
//  WriteReviewViewController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/31/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit
import FirebaseDatabase

class WriteReviewViewController: UIViewController {

    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var ratingField: UITextField!
    @IBOutlet weak var reviewText: UITextView!
    
    var currUser: User!
    var restaurant: Restaurant!
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func postButtonTapped(_ sender: UIButton) {
        ref = Database.database().reference()
        
        //make sure that all fields are filled out and that valid data is inputted
        if titleField.text != "" && ratingField.text != "" && reviewText.text != "", let rating = Int(ratingField.text!) {
            let title = titleField.text
            let review = reviewText.text
            
            //make sure that rating is between 1 and 5
            if rating > 5 || rating < 1 {
                let alert = UIAlertController(title: "Invalid Rating", message: "Ratings must be between 1 and 5", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            ref.child("locations/\(currUser.location.replacingOccurrences(of: " ", with: ""))/\(restaurant.name.replacingOccurrences(of: " ", with: ""))/reviews/\(title!.replacingOccurrences(of: " ", with: ""))").setValue(["title": title!, "rating": rating, "reviewText": review!, "userEmail": currUser.email, "userName": currUser.fname])
            
            //let user know that their review has been saved before closing screen
            let alert = UIAlertController(title: "Review Posted", message: "Your review has been posted.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            //alert user that not all fields have been filled out
            let alert = UIAlertController(title: "Incomplete/Invalid", message: "Please make sure all fields are filled out with valid information", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        //alert user that data won't be saved
        let alert = UIAlertController(title: "Warning", message: "Your review has not been posted, all data will be lost. Would you still like to continue?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }

}
