//
//  FeedViewController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/25/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit
import FirebaseDatabase

class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var ref: DatabaseReference!
    var restaurants = [Restaurant]()
    var currUser: User!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
        self.navigationController?.title = "Feed"
        
        self.tabBarController?.title = "Feed"
        
        parseAndFill()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tabs = tabBarController as! BaseTabBarController
//        userLocation = tabs.currUser.location.replacingOccurrences(of: " ", with: "")
        currUser = tabs.currUser
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    
    //parses data from database and fills the restaurant array.
    func parseAndFill() {
        self.tableView.isHidden = false
        restaurants.removeAll()
        
        ref = Database.database().reference()
        
        //get user location
        let tabs = tabBarController as! BaseTabBarController
        let location = tabs.currUser.location.replacingOccurrences(of: " ", with: "")
        
        //fill restaurant array
        ref.child("locations/\(location)").observeSingleEvent(of: .value) { (snapshot) in
            if let restaurantDict = snapshot.value as? [String: Any] {
                
                //parse data
                for (_,data) in restaurantDict {
                    let restaurantInfo = data as! [String: Any]
                    let name = restaurantInfo["name"] as? String ?? ""
                    let address = restaurantInfo["address"] as? String ?? ""
                    let description = restaurantInfo["description"] as? String ?? ""
                    let dietType = restaurantInfo["dietType"] as? String ?? ""
                    let menuData = restaurantInfo["menu"] as! [String:Any]
                    
                    var reviews = [Review]()
                    
                    //restaurants don't need to have reviews
                    if let reviewData = restaurantInfo["reviews"] as? [String: Any] {
                        for (_,data) in reviewData {
                            let reviewItem = data as! [String: Any]
                            let rating = reviewItem["rating"] as! Int
                            let title = reviewItem["title"] as! String
                            let reviewText = reviewItem["reviewText"] as! String
                            let userEmail = reviewItem["userEmail"] as! String
                            let userName = reviewItem["userName"] as! String
                            reviews.append(Review(rating: rating, title: title, reviewText: reviewText, userEmail: userEmail, userName: userName))
                        }
                    }
                    
                    var menu = [Item]()
                    //parse menu
                    for (_,itemData) in menuData {
                        let item = itemData as! [String: Any]
                        let itemName = item["name"] as! String
                        let itemPrice = item["price"] as! Double
                        let itemDescription = item["description"] as! String
                        
                        menu.append(Item(name: itemName, price: itemPrice, description: itemDescription))
                    }
                    
                    let newRestaurant = Restaurant(name: name, address: address, description: description, dietType: dietType, menu: menu, reviews: reviews)
                    self.restaurants.append(newRestaurant)
                }
                
            }
            else {
                self.tableView.isHidden = true
            }
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table View Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseID", for: indexPath) as! RestaurantTableViewCell
        
        //configure
        cell.restaurnantName?.text = restaurants[indexPath.row].name
        cell.dietType?.text = restaurants[indexPath.row].dietType
        cell.menuItem.text = restaurants[indexPath.row].lowPrice + " - " + restaurants[indexPath.row].highPrice
        
        //return cell
        return cell
    }
    
    

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            let restaurantToSend = restaurants[indexPath.row]
           
            let destination = segue.destination as! RestaurantViewController
            destination.restaurant = restaurantToSend
            destination.currUser = currUser
        }
    }
 

}
