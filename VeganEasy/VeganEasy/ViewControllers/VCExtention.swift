//
//  VCExtention.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/29/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    //this funciton dismisses the keyboard if tapped anywhere but the keyboard and works for every view controller
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
