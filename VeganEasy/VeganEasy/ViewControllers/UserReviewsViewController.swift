//
//  UserReviewsViewController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/31/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit
import FirebaseDatabase

class UserReviewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    
    var ref: DatabaseReference!
    var restaurants = [Restaurant]()
    var userReviews = [Review]()
    var currUser: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tabs = tabBarController as! BaseTabBarController
        currUser = tabs.currUser
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.title = "Your Reviews"
        
        let location = currUser.location.replacingOccurrences(of: " ", with: "")
        
        //reset data so that duplicates aren't made
        userReviews.removeAll()
        
        ref = Database.database().reference()
        ref.child("locations/\(location)").observeSingleEvent(of: .value) { (snapshot) in
            if let restaurantDict = snapshot.value as? [String: Any] {
                
                //parse data
                for (_,data) in restaurantDict {
                    let restaurantInfo = data as! [String: Any]
                    let name = restaurantInfo["name"] as? String ?? ""
                    let address = restaurantInfo["address"] as? String ?? ""
                    let description = restaurantInfo["description"] as? String ?? ""
                    let dietType = restaurantInfo["dietType"] as? String ?? ""
                    
                    var reviews = [Review]()
                    
                    //restaurants don't need to have reviews
                    if let reviewData = restaurantInfo["reviews"] as? [String: Any] {
                        for (_,data) in reviewData {
                            let reviewItem = data as! [String: Any]
                            let rating = reviewItem["rating"] as! Int
                            let title = reviewItem["title"] as! String
                            let reviewText = reviewItem["reviewText"] as! String
                            let userEmail = reviewItem["userEmail"] as! String
                            let userName = reviewItem["userName"] as! String
                            reviews.append(Review(rating: rating, title: title, reviewText: reviewText, userEmail: userEmail, userName: userName))
                        }
                    }
                    
                    //pass empty array to avoid downloading unnecessary data
                    let menu = [Item]()
                    let newRestaurant = Restaurant(name: name, address: address, description: description, dietType: dietType, menu: menu, reviews: reviews)
                    self.restaurants.append(newRestaurant)
                }
                
            }
            for restaurant in self.restaurants {
                self.userReviews.append(contentsOf: restaurant.getReviewsByUser(userEmail: self.currUser.email))
            }
            self.tableview.reloadData()
        }
    }

    // MARK: - Table View methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userReviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseMe") as! ReviewTableViewCell
        
        let currReview = userReviews[indexPath.row]
        
        cell.title.text = currReview.title
        cell.name.text = currReview.userName
        cell.reviewText.text = currReview.reviewText
        
        //set stars
        switch currReview.rating {
        case 1:
            cell.starsImage.image = UIImage(named: "1starImage")
        case 2:
            cell.starsImage.image = UIImage(named: "2starImage")
        case 3:
            cell.starsImage.image = UIImage(named: "3starImage")
        case 4:
            cell.starsImage.image = UIImage(named: "4starImage")
        case 5:
            cell.starsImage.image = UIImage(named: "5starImage")
        default:
            print("Invalid rating on cell \(indexPath.row)")
        }
        
        return cell
    }

}
