//
//  LogInViewController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/26/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class LogInViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    var ref: DatabaseReference!
    var users = [String: Any]()
    
    var currUser: User!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        //check if there is a current user. If so, skip this screen to avoid downloading unnessisary data
        if currUser != nil {
            performSegue(withIdentifier: "segueToTabControl", sender: self)
        }
        
        //get list of users
        ref = Database.database().reference()
        
        ref.child("users").observeSingleEvent(of: .value) { (snapshot) in
            self.users = (snapshot.value as? [String: Any])!
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == 0 {
            let nextTextField = textField.superview?.viewWithTag(textField.tag + 1)
            nextTextField?.becomeFirstResponder()
        }
        else if textField.tag == 1 { 
            self.resignFirstResponder()
            if self.shouldPerformSegue(withIdentifier: "segueToTabControl", sender: self) {
                self.performSegue(withIdentifier: "segueToTabControl", sender: self)
            }
        }
        
        return true
    }

    
    // MARK: - Navigation
 
    //checks if email and password are stored in database. If not, alert is displayed.
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        let email = emailField.text
        let password = passwordField.text

        for (_, data) in users {
            let thisUser = data as! [String: Any]
            let currEmail = thisUser["email"] as! String
            let currPassword = thisUser["password"] as! String
            let fname = thisUser["fname"] as! String
            let lname = thisUser["lname"] as! String
            let location = thisUser["location"] as! String

            if (email == currEmail && password == currPassword) {
                print("log in: " + currEmail)
                currUser = User(fname: fname, lname: lname, location: location, email: email!)
                return true
            }
            else {

                continue
            }
        }
        let alert = UIAlertController(title: "User Not Found", message: "Email/password combination not found. Try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! BaseTabBarController
        destination.currUser = self.currUser
    }

}
