//
//  BaseTabBarController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/27/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit

class BaseTabBarController: UITabBarController {
    
    var currUser: User!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
