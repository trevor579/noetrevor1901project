//
//  MenuItemViewController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/27/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit
import FirebaseDatabase

class MenuItemViewController: UIViewController {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    
    //stuff to change if editing
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addOrSaveButton: UIButton!
    @IBOutlet weak var finishButton: UIButton!
    var isEditingHappening: Bool!
    
    var didAdd = false
    
    var ref: DatabaseReference!
    
    var userLocation: String!
    
    var newRestaurant: Restaurant!
    var menuItem: Item!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        if isEditingHappening != nil && isEditingHappening == true {
            titleLabel.text = "Edit Menu Item"
            addOrSaveButton.setTitle("Save Item", for: .normal)
            finishButton.setTitle("Cancel", for: .normal)
        }
        
        //check if a menu item was sent
        if menuItem != nil {
            nameField.text = menuItem.name
            priceField.text = menuItem.price.description
            descriptionField.text = menuItem.description
        }
    }
    
    //cancels menu item addition and dismisses the view controller
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        
        if didAdd {
            self.dismiss(animated: true, completion: nil)
        } else {
            //alert user that data will not be saved if they continue
            let alert = UIAlertController(title: "Warning", message: "You haven't added any menu items! You must add at least one menu item to the restaurant to save it. Are you sure you want to continue?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    //save the new item to the restaurant and save the restaurant/menu to database
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        
        if nameField.text != "" && priceField.text != "" && descriptionField.text != "" {
            
            //add new menu item
            newRestaurant.menu.append(Item(name: nameField.text!, price: Double(priceField.text!)!, description: descriptionField.text))
            
            //Save restaurant data to database
            //name for database listing
            let dbName = newRestaurant.name.replacingOccurrences(of: " ", with: "")
            ref = Database.database().reference()
            ref.child("locations/\(userLocation!)/\(dbName)").setValue(["name" : newRestaurant.name, "address" : newRestaurant.address, "description" : newRestaurant.description, "dietType" : newRestaurant.dietType])
            
            for item in newRestaurant.menu {
                //name for database listing for menu item
                let dbItem = item.name.replacingOccurrences(of: " ", with: "")
                ref.child("locations/\(userLocation!)/\(dbName)/menu/\(dbItem)").setValue(["name": item.name, "description": item.description, "price": item.price])
            }
            
            //if editing, dismiss when save button pressed
            if isEditingHappening != nil && isEditingHappening == true {
                self.dismiss(animated: true, completion: nil)
            }
            
            //give user alert to let them know that the restaurant was added
            let alert = UIAlertController(title: "Item Saved", message: "Your item has been saved to the restaurant's menu.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            //clear fields for multiple menu item inputs
            nameField.text = ""
            priceField.text = ""
            descriptionField.text = ""
            
            didAdd = true
            
        } else {
            //alert user that they must fill everything out
            let alert = UIAlertController(title: "Incomplete", message: "Please fill out all text fields", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
