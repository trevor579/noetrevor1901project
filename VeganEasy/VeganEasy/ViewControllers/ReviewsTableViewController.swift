//
//  ReviewsTableViewController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/31/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ReviewsTableViewController: UITableViewController {
    
    var restaurant: Restaurant!
    var reviews: [Review]!
    var currUser: User!
    var ref: DatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        reviews = restaurant.reviews
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let userLocation = currUser.location.replacingOccurrences(of: " ", with: "")
        
        //make sure that all information is up to date
        restaurant.reviews.removeAll()
        reviews.removeAll()
        ref = Database.database().reference()
        ref.child("locations/\(userLocation)/\(restaurant.name.replacingOccurrences(of: " ", with: ""))/reviews").observeSingleEvent(of: .value) { (snapshot) in
            if let reviewsData = snapshot.value as? [String: Any] {
                for (_,data) in reviewsData {
                    let review = data as! [String: Any]
                    let rating = review["rating"] as! Int
                    let reviewText = review["reviewText"] as! String
                    let title = review["title"] as! String
                    let userEmail = review["userEmail"] as! String
                    let userName = review["userName"] as! String
                    
                    let newReview = Review(rating: rating, title: title, reviewText: reviewText, userEmail: userEmail, userName: userName)
                    self.restaurant.reviews.append(newReview)
                }
            }
            self.reviews = self.restaurant.reviews
            self.tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as! ReviewTableViewCell

        // Configure the cell...
        cell.title.text = reviews[indexPath.row].title
        cell.name.text = reviews[indexPath.row].userName
        cell.reviewText.text = reviews[indexPath.row].reviewText
        
        //set stars
        switch reviews[indexPath.row].rating {
        case 1:
            cell.starsImage.image = UIImage(named: "1starImage")
        case 2:
            cell.starsImage.image = UIImage(named: "2starImage")
        case 3:
            cell.starsImage.image = UIImage(named: "3starImage")
        case 4:
            cell.starsImage.image = UIImage(named: "4starImage")
        case 5:
            cell.starsImage.image = UIImage(named: "5starImage")
        default:
            print("Invalid rating on cell \(indexPath.row)")
        }

        return cell
    }

    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! WriteReviewViewController
        destination.currUser = currUser
        destination.restaurant = restaurant
    }
    

}
