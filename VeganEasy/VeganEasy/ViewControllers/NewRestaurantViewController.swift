//
//  NewRestaurantViewController.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/27/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit

class NewRestaurantViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var segControl: UISegmentedControl!
    
    var userLocation: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
        // Do any additional setup after loading the view.
    }

    
    // MARK: - Navigation

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if nameField.text == "" || addressField.text == "" || descriptionField.text == "" {
            let alert = UIAlertController(title: "Incomplete", message: "Please fill out all text fields", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if nameField.isEditing {
            addressField.becomeFirstResponder()
        }
        else if addressField.isEditing {
            descriptionField.becomeFirstResponder()
        }
        else {
            self.performSegue(withIdentifier: "segueToAddItems", sender: self)
        }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destination = segue.destination as! MenuItemViewController
        
        destination.userLocation = userLocation
        
        //decide the dietType based on selected segment control
        if segControl.selectedSegmentIndex == 0 {
            
            //create a new restaurant
            let name = nameField.text!
            let address = addressField.text!
            let description = descriptionField.text!
            let dietType = "Vegan"
            let reviews = [Review]()
            let menu = [Item]()
            
            //clear fields so that users can input another restaurant if they want.
            nameField.text = ""
            addressField.text = ""
            descriptionField.text = ""
            segControl.selectedSegmentIndex = 0
            
            let newRestaurant = Restaurant(name: name, address: address, description: description, dietType: dietType, menu: menu, reviews: reviews)
            destination.newRestaurant = newRestaurant
        }
        else if segControl.selectedSegmentIndex == 1 {
            
            let name = nameField.text!
            let address = addressField.text!
            let description = descriptionField.text!
            let dietType = "Vegan Friendly"
            let reviews = [Review]()
            let menu = [Item]()
            
            //clear fields so that users can input another restaurant if they want.
            nameField.text = ""
            addressField.text = ""
            descriptionField.text = ""
            segControl.selectedSegmentIndex = 0
            
            let newRestaurant = Restaurant(name: name, address: address, description: description, dietType: dietType, menu: menu, reviews: reviews)
            destination.newRestaurant = newRestaurant
        }
        else {
            print("Error: Invalid segment control value selected")
        }
    }
 

}
