//
//  User.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/27/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import Foundation

class User {
    
    var fname: String
    var lname: String
    var location: String
    var email: String
    
    var fullName: String {
        get {
            return "\(fname) \(lname)"
        }
    }
    
    init(fname: String, lname: String, location: String, email: String) {
        self.fname = fname
        self.lname = lname
        self.location = location
        self.email = email
    }
}
