//
//  Review.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/31/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import Foundation

class Review {
    var rating: Int
    var title: String
    var reviewText: String
    var userEmail: String
    var userName: String
    
    var titleToSave: String {
        get {
            return title.replacingOccurrences(of: " ", with: "")
        }
    }
    
    init(rating: Int, title: String, reviewText: String, userEmail: String, userName: String) {
        self.rating = rating
        self.title = title
        self.reviewText = reviewText
        self.userEmail = userEmail
        self.userName = userName
    }
}
