//
//  RestaurantTableViewCell.swift
//  VeganEasy
//
//  Created by Trevor Noe on 1/26/19.
//  Copyright © 2019 Trevor Noe. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell {

    @IBOutlet weak var restaurnantName: UILabel!
    @IBOutlet weak var dietType: UILabel!
    @IBOutlet weak var menuItem: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
